<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
        private $_id;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		/* $users=array(
			// username => password
			'demo'=>'demo',
			'admin'=>'admin',
		);
		if(!isset($users[$this->username]))
                        $this->errorCode=self::ERROR_USERNAME_INVALID;
                elseif($users[$this->username]!==$this->password)
                        $this->errorCode=self::ERROR_PASSWORD_INVALID;
                else
                        $this->errorCode=self::ERROR_NONE;
                return !$this->errorCode;  */

		$model = Tblpengguna::model()->find('stafnokp=?',array($this->username));
		
		if($model===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		elseif(!$model->validatePassword($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else{
			$this->_id=$model->penggunaid;
			//$this->setState('role_id',$model->role_id);
			$model->penggunaloginakhir=date('Y-m-d H:m:s');
			$model->save(false);
			$this->errorCode=self::ERROR_NONE;
		}
		return !$this->errorCode; 
	}
	
	public function getId()
	{
		return $this->_id;
	}  
}
