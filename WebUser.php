<?php
 
// this file must be stored in:
// protected/components/WebUser.php
 
class WebUser extends CWebUser {

  // Store model to not repeat query.
  private $_model;
 
  // This is a function that checks the field 'role'
  // in the User model to be equal to 1, that means it's admin
  // access it by Yii::app()->user->isAdmin()
  public function getIsAdmin(){
    $user = $this->loadUser(Yii::app()->user->id);
    return isset($user->perananpgunakod)?($user->perananpgunakod == '01'):false;
  }
  
  public function getIsUser(){
    $user = $this->loadUser(Yii::app()->user->id);
    return isset($user->perananpgunakod)?($user->perananpgunakod == '02'):false;
  }
 
  public function getRole_Id(){
     $user = $this->loadUser(Yii::app()->user->id);
     return isset($user->perananpgunakod)?$user->perananpgunakod:null;
  }
  
  // Load user model.
  protected function loadUser($id=null)
    {
        if($this->_model===null)
        {
            if($id!==null)
                $this->_model=Tblcapaianpengguna::model()->findByPk($id);
        }
        return $this->_model;
    }
}
?>
